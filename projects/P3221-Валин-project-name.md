# Сайт-библиотека "MyAnimeLibrary"
## Автор
P3221 Валин Никита Юрьевич

## Описание
Сервис позволяет владельцу создавать личный каталог аниме, манги, ранобэ и т.д. с различными пометками(например добавить дату выхода новой серии, уведомление о которой автоматически придет на почту), позволяет писать рецензии(их можно как выложить в открытый доступ, так и оставить видимыми только для себя), содержит различные топы по всевозможным категориям, на основании просмотренных произведений будут формироваться еже(-дневные, -недельные) рекомендации. В будущем возможно добавление дополнительного функционала(Может быть будет реализовано еже(-недельное, -месячное) голосование, на основе которого будет добавлена та или иная функция).
Предусмотрены также роли:
гостя(человек, который не авторизировался на сайте) - может просматривать весь возможный контент, но не имеет права пользоваться функционалом сервиса, таким как: создавать свою библиотеку, писать рецензии, и т.п.
Возможна реалзация таких ролей, как Вип пользователь, у которого будет возможность использовать экслюзивные возможности сервиса.

## Детали
- Продукт - веб-приложение c базой данных, пользовательский интерфейс - веб-страницы.
- Стек технологий - Spring MVC, MySQL, Thymeleaf.(Возможны изменения?)